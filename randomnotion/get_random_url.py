from notion.client import NotionClient
import random

def get_random_url(token, collection_view_url):
  client          = NotionClient(token_v2=token)
  cv              = client.get_collection_view(collection_view_url)
  collection_rows = cv.default_query().execute()

  if len(collection_rows) != 0:
    random_element       = random.choice(collection_rows)
    random_element_id    = random_element.id
    random_element_title = random_element.title
    random_element_url   = build_random_element_url(collection_view_url, random_element_id, random_element_title)

    return {
      "rsp": {
        "url":   random_element_url,
        "title": random_element.title,
      }
    }
  else:
    return { "rsp": None }


def build_random_element_url(collection_view_url, element_id, random_element_title):
  notion_base_url           = "/".join(collection_view_url.split("/")[:-1])
  element_id_without_dashes = element_id.replace("-", "")

  return f'{notion_base_url}/{element_id_without_dashes}'
