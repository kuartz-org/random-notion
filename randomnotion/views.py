from flask        import Flask, jsonify, request
from flask_cors   import CORS
from randomnotion import get_random_url

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
  token_v2            = request.args.get('token')
  collection_view_url = request.args.get('url')
  random_url_rsp      = get_random_url.get_random_url(token_v2, collection_view_url)
  json_rsp            = jsonify(random_url_rsp)

  return json_rsp
